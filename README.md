# CTIDE-scripts

Set of scripts and job options for handling testing of new neural networks in CTIDE code. These are eventually destined for distribution to students/users and are not the same as ongoing Athena development.

## Notes from PF to guide this

[We] were suggesting to use Reco_tf.py, but, as discussed, this could be an overhead as for NN testing we are only interested in InDet reco only (at least in basic tests, then final tests for production shall run in RecoTf.)

There are some examples on how to run a InDetOnly reconstruction. 
This is what alignment uses:
https://gitlab.cern.ch/atlas/athena/blob/master/InnerDetector/InDetExample/InDetAlignExample/share/jobOption_RecExCommon.py
And runs on RAW / RDOs. 

There are also other possible jobOptions to use
This,for example, seems a slimmed down version of the Alignment one. Seems to me that should work relatively out of the box, but haven't tested lately (I mostly used Athena to run on ESD lately).
https://gitlab.cern.ch/atlas/athena/blob/master/InnerDetector/InDetExample/InDetRecExample/share/jobOptions_RecExCommon.py

For loading a .root file without converting to local sqlite, this should do the job.
https://gitlab.cern.ch/atlas/athena/blob/master/InnerDetector/InDetExample/InDetAlignExample/share/jobOption_ConditionsOverrider.py#L77
(These seems the lines that load a pool.root file. For example that is what should be used for alignment constants. Matthias shall correct me if I'm wrong).

## Useful info

How to use InDetRecExample:
https://gitlab.cern.ch/atlas/athena/blob/master/InnerDetector/InDetExample/InDetRecExample/doc/packagedoc.h

## Notes on our plan

- Definitely want an InDet only example: keep this as lightweight as we can while still working correctly.
- Ideally, include InDetRecExample's jobOptions_RecExCommon in our own job options, where we can specify set of CTIDE-relevant input files and output properties in addition to baseline contents
- Examine the root file loader. Can we use this directly with what Sourav has? (Or JSON once we switch, of course.)
- Determine how to set extra Athena flags for turning options on/off via job options. Add a simple option to test (cout statement)
