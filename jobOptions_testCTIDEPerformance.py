from AthenaCommon.AthenaCommonFlags import athenaCommonFlags

# What do we want to test on?
# Note for future: do we get one root file per job here, or would it lump everything together?
inputFiles = ["/home/kpachal/project/kpachal/Datasets_CTIDE/fromSourav/user.rjansky.mc16_13TeV.427080.Pythia8EvtGen_A14NNPDF23LO_flatpT_Zprime.RDO_20180110_EXT0//user.rjansky.12944101.EXT0._000004.RDO.root"]

# Automatically outputs an ESD and AOD, so nothing further needed there.

# What overrides do we want to do?
# For example, local database testing, sending flags to our algorithms, and so forth.
testCTIDENNFile = "/home/kpachal/CTIDE/AthenaDevelopment/run/files/cond09_mc.000087.gen.COND._0004.pool.root"

# Last step: load common job options
# and overwrite with anything I set above.
include ("InDetAlignExample/jobOption_RecExCommon.py")
include("InDetAlignExample/jobOption_ConditionsOverrider.py")

# Overrides which aren't part of the Conditions Overrider
from EventSelectorAthenaPool.EventSelectorAthenaPoolConf import CondProxyProvider
from AthenaCommon.AppMgr import ServiceMgr
if testCTIDENNFile :

  # Block where it has been collecting this info from
  from IOVDbSvc.CondDB import conddb
  conddb.blockFolder("/PIXEL/PixelClustering/PixelClusNNCalib")

  ServiceMgr += CondProxyProvider()
  ServiceMgr.ProxyProviderSvc.ProviderNames += [ "CondProxyProvider" ]
  # set this to the file containing your trained NN.
  # For now we hard code it in this job options, but later we can take
  # it as an argument to make this easier to run on.
  ServiceMgr.CondProxyProvider.InputCollections += [testCTIDENNFile]
  print "INPUT POOL FILES COLLECTION", ServiceMgr.CondProxyProvider.InputCollections 
  ServiceMgr.CondProxyProvider.OutputLevel = DEBUG
  print ServiceMgr.CondProxyProvider
  # this preload causes callbacks for read in objects to be activated,
  # allowing GeoModel to pick up the transforms
  ServiceMgr.IOVSvc.preLoadData = True
  ServiceMgr.IOVSvc.OutputLevel = DEBUG

else :
  print "NOT DOING THE THING"
